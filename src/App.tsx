import React, { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "./store/hooks";
import { Loader } from "./components/loader/Loader";
import { Header } from "./components/header/Header";
import { fetchChannels } from "./store/channels/channelsSlice";
import { Button } from "./components/button/Button";
import "./App.scss";
import { Channel as ChannelDto } from "./types/Channel";
import { Channel } from "./components/channel/Channel";
import { Footer } from "./components/footer/Footer";
import { IconButton } from "./components/button/IconButton";
import { ConnectionError } from "./components/error/ConnectionError";
import Arrow from "./assets/ic-arrow.svg";

function App() {
	const dispatch = useAppDispatch();
	const { value, status } = useAppSelector((state) => state.channels);
	const [activeTheme, setActiveTheme] = useState("1");
	const [activeChannels, setActiveChannels] = useState<ChannelDto[] | null>(
		null
	);
	const [slice, setSlice] = useState(3);
	const [width, setWidth] = useState<number>(window.innerWidth);

	function handleWindowSizeChange() {
		setWidth(window.innerWidth);
	}

	useEffect(() => {
		window.addEventListener("resize", handleWindowSizeChange);

		return () => {
			window.removeEventListener("resize", handleWindowSizeChange);
		};
	}, []);

	const isMobile = width <= 640;

	useEffect(() => {
		dispatch(fetchChannels());
	}, [dispatch]);

	/**
	 * Получает компонент тем каналов. В зависимости от состояния стора,
	 * получающего/хранящего данные, отображает спиннер, ошибку или данные в виде
	 * кнопок-категорий
	 */
	const themes =
		status === "loading" ? (
			<Loader size={2} color={"white"} />
		) : status === "failed" ? (
			<div>
				<ConnectionError>Что-то пошло не так, попробуйте еще раз</ConnectionError>
			</div>
		) : (
			Object.entries(value)?.map(([themeId, { theme, channels }]) => (
				<Button
					key={themeId}
					onClick={() => {
						setActiveTheme(themeId);
						setActiveChannels(channels);
					}}
					type={themeId === activeTheme ? "primary" : "secondary"}
				>
					{theme.plural || theme.name}
				</Button>
			))
		);

	/**
	 * Получает компонент каналов. Ничего не отображает, пока не получит данные
	 * из стора
	 */
	const channels =
		status === "loading" || status === "failed"
			? null
			: (
				activeChannels?.slice(0, slice) ||
				value["1"]?.channels.slice(0, slice)
			)?.map(({ alias, title, logo, description, url, hd, button, chid }) => (
				<Channel
					{...{
						key: chid,
						alias,
						title,
						logo,
						description,
						url,
						hd,
						button,
					}}
				/>
			));

	/**
	 * Кнопка "Отобразить еще", изменяется в зависимости от устройства (desktop/mobile).
	 * Признак типа устройства контролирует хук и событие "resize"
	 */
	const buttonMore = isMobile ? (
		<Button
			color="#FF4747"
			type="primary"
			onClick={() => {
				setSlice(slice + 3);
			}}
		>
			Показать еще
		</Button>
	) : (
		<IconButton
			onClick={() => {
				setSlice(slice + 3);
			}}
			className="App__btn-more"
		>
			<img src={Arrow} alt="arrow"></img>
		</IconButton>
	);

	return (
		<div className="App">
			<Header />
			<main>
				<h1>Телеканалы</h1>
				<div>{themes}</div>
				<div className="App__channels">
					{channels}
					{status === "idle" &&
						((activeChannels || value["1"]?.channels)?.length || 0) > 3 &&
						buttonMore}
				</div>
			</main>
			<Footer />
		</div>
	);
}

export default App;
