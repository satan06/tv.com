import { Channel } from "../types/Channel";
import { Theme } from "../types/Theme";
import { ThemedChannelMap } from "../types/ThemedChannelMap";

export const apiHost = "https://epg.domru.ru";

const getApi = (url: string) =>
	fetch(url, {
		method: "GET",
		mode: "cors",
		headers: {
			"Content-Type": "application/json",
		},
	});

export async function getChannels(
	{ domain }: { domain: string } = { domain: "ekat" }
): Promise<Channel[]> {
	const url = `${apiHost}/channel/list?domain=${domain}`;

	return await getApi(url)
		.then((x) => x.json())
		.then((x: Array<Omit<Channel, "hd"> & { hd: string }>) =>
			x.map<Channel>((y) => ({
				...y,
				hd: Boolean(y.hd),
			}))
		);
}

export async function getThemes(): Promise<Theme[]> {
	const url = `${apiHost}/channeltheme/list`;

	return await getApi(url)
		.then((x) => x.json())
		.then((x: Array<Theme>) => x);
}

export async function getThemedChannels(
	{ domain }: { domain: string } = { domain: "ekat" }
): Promise<ThemedChannelMap> {
	return await Promise.all([getChannels({ domain }), getThemes()]).then(
		([channels, themes]) => {
			const result: ThemedChannelMap = {};

			for (const theme of themes) {
				const id = theme.thid;

				result[id] = {
					theme,
					channels: channels.filter((x) => x.thid === id),
				};
			}

			return result;
		}
	);
}
