import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import channelReducer from './channels/channelsSlice';

export const store = configureStore({
  reducer: {
    channels: channelReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
