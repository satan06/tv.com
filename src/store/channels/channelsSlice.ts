import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import type { RootState } from '../../store';
import { ThemedChannelMap } from '../../types/ThemedChannelMap';
import { getThemedChannels } from '../../utils/api';

export type ChannelsState = {
  value: ThemedChannelMap,
  status: 'loading' | 'idle' | 'failed'
};
  
const initialState: ChannelsState = {
  value: {},
  status: 'idle'
};

export const fetchChannels = createAsyncThunk(
    'channels/fetchChannels',
    getThemedChannels
);

export const channelsSlice = createSlice({
    name: 'channels',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchChannels.pending, (state) => {
          state.status = 'loading';
        })
        .addCase(fetchChannels.fulfilled, (state, action) => {
          state.status = 'idle';
          state.value = action.payload;
        })
        .addCase(fetchChannels.rejected, (state) => {
          state.status = 'failed';
        });
    }
})

export const selectChannels = (state: RootState) => state.channels.value;

export default channelsSlice.reducer;