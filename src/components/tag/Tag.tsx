import React from "react";
import "./Tag.scss";

type TagProps = {
	children: React.ReactNode;
	color: string;
	expanded?: Boolean;
	expandedText?: string;
};

export function Tag({ children, color, expanded, expandedText }: TagProps) {
	if (!expanded) {
		return (
			<div
				style={{
					backgroundColor: color,
				}}
				className="tag"
			>
				<span>{children}</span>
			</div>
		);
	}

	return (
		<div className="tag tag--expanded">
			<span>{children}</span>
			<div
				style={{
					backgroundColor: color,
				}}
				className="tag tag--expanded__text"
			>
				<span
					style={{
						fontWeight: 600,
						fontSize: "13px",
						lineHeight: "16px",
					}}
				>
					{expandedText}
				</span>
			</div>
		</div>
	);
}
