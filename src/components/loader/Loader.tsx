import React from "react";
import "./Loader.scss";

export function Loader({
	size,
	color,
}: Partial<{ size: number; color: string }>) {
	if (!size) {
		size = 4;
	}

	if (!color) {
		color = "red";
	}

	return (
		<div
			className="loader"
			style={{
				fontSize: `${size}rem`,
			}}
		>
			<svg width="1em" height="1em">
				<circle
					className="loader__circle"
					style={{ stroke: color }}
					cx="0.5em"
					cy="0.5em"
					r="0.45em"
				/>
			</svg>
		</div>
	);
}
