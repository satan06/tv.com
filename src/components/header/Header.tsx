import React from "react";
import logo from '../../assets/logo.png';
import { IconButton } from '../../components/button/IconButton';
import './Header.scss';
import User from '../../assets/ic-user.svg'

export function Header() {
    return (
        <header className="header">
            <img className="header--logo" src={logo} color="black" alt="tv.com"/>
            <IconButton>
                <img src={User} alt="user"></img>
            </IconButton>    
        </header>
    );
}