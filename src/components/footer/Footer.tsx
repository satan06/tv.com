import React from "react";
import logo from "../../assets/logo.png";
import "./Footer.scss";

export function Footer() {
	return (
		<footer className="footer">
			<div className="footer__inner">
				<div className="footer__inner__info">
					<img
						className="footer__inner--logo"
						src={logo}
						color="black"
						alt="tv.com"
					/>
					<div className="footer__inner__info__links">
						<ul>
							<li>Раскрытие информации</li>
							<li>Закупки</li>
						</ul>
						<ul>
							<li>Лицензии и сертификаты</li>
							<li>Сообщить о проблеме</li>
						</ul>
					</div>
				</div>
				<div className="footer__inner__copyright">
					<p className="footer__inner__copyright__name">
						© 2022 ПАО «TV.COM». 18+
					</p>
					<p className="footer__inner__copyright__desc">
						Продолжая использовать наш сайт, вы даете согласие на обработку
						файлов Cookies и других пользовательских данных, в соответствии с
						Политикой конфиденциальности и Пользовательским соглашением
					</p>
				</div>
			</div>
		</footer>
	);
}
