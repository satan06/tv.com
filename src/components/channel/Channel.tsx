import React from "react";
import { Channel as ChannelDto } from "../../types/Channel";
import { apiHost } from "../../utils/api";
import { Tag } from "../tag/Tag";
import { ChannelImg } from "../image/ChannelImg";
import "./Channel.scss";

export function Channel({
	title,
	logo,
	description,
	url,
	hd,
	button,
	alias,
}: Pick<
	ChannelDto,
	"title" | "logo" | "description" | "url" | "hd" | "button" | "alias"
>) {
	const placeholder = require("../../assets/broadcast.png");

	return (
		<div className="channel">
			<div className="channel__header">
				<div className="channel__header__logo">
					<div className="channel__header__logo__inner">
						<ChannelImg
							width={54}
							height={54}
							src={apiHost + logo}
							alt={alias}
							placeholder={placeholder}
						/>
					</div>
				</div>
				<div className="channel__header__info">
					<a href={url}>{title}</a>
					{hd ? <Tag color="#FF4747">Цифровой</Tag> : null}
					<Tag expanded color="#FF4747" expandedText={button || '00'}>
						Кнопка на пульте
					</Tag>
				</div>
			</div>
			<div className="channel__body">{description}</div>
		</div>
	);
}
