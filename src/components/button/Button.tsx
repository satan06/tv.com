import React from "react";
import "./Button.scss";

export type ButtonProps = {
	type: "primary" | "secondary";
	children: React.ReactNode;
	onClick?: React.MouseEventHandler<HTMLButtonElement>;
	color?: string;
};

export function Button({ children, type, onClick, color }: ButtonProps) {
	return (
		<button
			style={{ background: color }}
			onClick={onClick}
			className={`button button--${type}`}
		>
			{children}
		</button>
	);
}
