import React from "react";
import "./Button.scss";

export function IconButton({
	children,
	className,
	onClick,
}: {
	children: React.ReactNode;
	className?: string;
	onClick?: React.MouseEventHandler<HTMLDivElement>;
}) {
	return (
		<div onClick={onClick} className={"button--icon " + (className || "")}>
			{children}
		</div>
	);
}
