import React from "react";
import ErrorImage from '../../assets/error.png';
import "./ConnectionError.scss";
import { Button } from "../button/Button";

export function ConnectionError({ children }: { children: React.ReactNode }) {
    return (
        <div className="connection-error">
            <img src={ErrorImage} alt="connection-error" />
            <span>{children}</span>
            <Button
                onClick={() =>
                    window.location.reload()
                }
                type="primary"
            >
                <span>Попробовать еще</span>
            </Button>
        </div>
    )
}