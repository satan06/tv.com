import React, { useState, useEffect } from "react";

export function ChannelImg({
	placeholder,
	src,
	alt,
	width,
	height,
}: {
	placeholder: string;
	src: string;
	alt: string;
	width: number;
	height: number;
}) {
	const [imgSrc, setImgSrc] = useState(placeholder || src);

	useEffect(() => {
		const img = new Image();
		img.src = src;

		img.onload = () => {
			setImgSrc(src);
		};
	}, [src]);

	return <img {...{ src: imgSrc, width, height }} alt={alt || ""} />;
}
