import { Channel } from './Channel';
import { Theme } from './Theme';

export type ThemeId = Theme['thid'];

export type ThemedChannelMap = Record<ThemeId, {
    theme: Omit<Theme, 'thid'>;
    channels: Channel[];
}>;