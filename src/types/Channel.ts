export type Channel = {
    chid: string,
    title: string,
    biglogo: string | null,
    logo: string,
    description: string,
    url: string,
    hd: boolean,
    thid: string,
    display: string,
    alias: string,
    xvid: string,
    epgId: string,
    button: string,
}