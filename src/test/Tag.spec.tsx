import * as React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { Tag } from '../components/tag/Tag';

let documentBody: RenderResult;

describe('<Tag />', () => {
  beforeEach(() => {
    documentBody = render(<Tag color="red">TagContent</Tag>);
  });
  it('shows not found message', () => {
    expect(documentBody.getByText('TagContent')).toBeInTheDocument();
  });
});